### Plot
A girl finds a mysterious object that gives her the power to change alternate universes in her dreams
to become the new canon universe after she's forced to move in with a friend when her parent's are arrested.

She has to learn to use her powers to discover the giant secret her parents were keeping
and save them before things get worse.