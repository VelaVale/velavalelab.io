# Setting
An alternate Earth (but still set in 2016 as of the beginning of the story)
where a lot of towns decided to move onto artificially-created Metal Hills that provide
- Constant warm climates
- Less power issues
- A great view of a starry purple sky (instead of blue).

The hills are covered with Turf, Concrete, and other things to make it seem more like normal towns.

Other towns didn't like the idea and wanted to keep Earth pristine.
These are usually overshadowed by the hills which makes them darker and colder the majority of the time.

## Gear Hill
One of the Metal Hills and is the main setting of VelaVale. It has multiple businesses ranging from restraunts, stores, and much more.

## Nexultic
A "pristine" town that's overshadowed by Gear Hill on the left and another hill on the right.
It's a simplistic farming town with no stores, 1 family-run dinner, and a shoe factory.

Because of the hills causing less sun and heat, farmers had to get creative and made hybrid plants that can survive in the harsh climate.