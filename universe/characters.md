# Characters
## Cosmcala Vela
The 8-year-old female main character who gains AU Altering powers in her dreams after finding an object.
Her hometown is Nexultic and she is homeschooled.

Her close friends call her "Cosi" for short.

### Personality
- Shy
- Willing to do anything to protect her friends and family
- More mature for her age
- Get's scared easily

### Look
- Normal Outfit
    - Light blue skirt
    - Purple blouse
    - Black sneakers
- Body
    - Some baby fat
    - Emerald green eyes 
    - Slick Brown hair
- AU/Dream Self Changes
    - Black eyes
    - Pink and Green "Tentacle" limbs instead of human arms/legs

## Catty Chromatic
Cosi's Friend, a 10-year-old girl. Her hometown is Gear Hill.

### Job
In this alternate Earth, when you hit 10 you are required to get a job.
She works as a comedian and tells tons of bad puns.

### Personality
- Comedic
- Sweet
- Outgoing

### Looks
She's constantly dying her hair color and coming up with new clothes combos.
- Normal Outfit
    - Rainbow t-shirt
    - Blue jeans
    - Rose gold high-heels
- Body
    - Skinny
    - Blue eyes
    - Ginger Hair