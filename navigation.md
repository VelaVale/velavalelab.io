# VelaVale

[gimmick:theme](paper)

[Home](index.md)

[The Universe]()

  * [Plot](universe/plot.md)
  * [Characters](universe/characters.md)
  * [Setting](universe/setting.md)

[The Concept]()

  * [Guidelines, Rules, and Rights](how_it_works/guidelines.md)
  * [Components and Schedule](how_it_works/howitworks.md)

[Get Started](how_it_works/getstarted.md)