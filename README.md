# VelaVale Info
VelaVale is a collaborative Mystery and Adventure story that acts more like a TV show than a written story.
Each episode is written by multiple people and voted into the canon universe.

## Table of Contents
### The Universe
- Plot
- Characters
- Setting

### How it works
- Guidelines
- Schedule