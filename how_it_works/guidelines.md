# Guidelines
To make everything consistent, we have a few optional guidelines and required rules to follow.

### Required Rules
- Don't change existing details (including ones past episodes added) just because you don't like it as once it's canon, it's canon.
    (This means that stuff can't be changed without a reason but an event could happen that changes something.
    For instance, Catty's job is a comedian but maybe she got fired for some reason so her job changes.)
- No NSFW or fetish stuff allowed as it's for all ages plus they are kid characters.
- Follow Story Wars' Rules: https://www.storywars.net/rules

### Optional Guidelines
- The overarching story doesn't need to be apart of each episode. Some episodes can be fun "B-plots"
- Try to keep the a medium length for your part as they will be combined to make the episode.
    (A novel for each part is way to much when combined but also don't make it a few words or sentences only.)

Other than that... any new details, characters, settings, and each episode's plot is up to what you write and what the voters choose!
Anything that loses the vote can be turned into an AU.

## Rights
The universe belongs to me but the story belongs to anybody participating (by writing, voting, etc.)
under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This means you are free to share and change anything about the story as long as you credit me.
You can even sell it (I wouldn't advise it but you are technically allowed to).

As for the universe:
- If it's for the story, the above rules apply.
- If you want to use them for fan content, please link back to the story, share with me the link (I would love to see it!), and you are not allowed to sell it. (Two exceptions: ads on fan videos/websites doesn't count as selling it and you can sell it with an optional donation (aka Name-Your-Own-Price) if you still provide it for free without the donation) 
- If you want to use the characters in anything else, permission is required!