# How to Contribute
1. Read this info and make sure you are caught up with finished episodes.
2. [Sign Up or Log into an existing Story Wars account](https://storywars.net)
3. [Find the current episode by visiting the VelaVale page](http://velavale.brandongiesing.com)
4. Follow the processes listed in the How It Works page.