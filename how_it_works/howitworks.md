# How VelaVale Works
VelaVale is made up of components based on how TV Shows work but adapted for Text.
- Scenes: Indivdual segments that make up each episode and are by different creators.
- Episodes: 7 Scenes[^1] that combined share a common theme/goal
- Seasons: 25 episodes combined that tell a story arc.
- Series: All the story arcs combined.

## Scenes
When an episode is started, the writing process begins:
1. Anybody can submit a segment anonymously for that episode (that continues from the last) until we fill it.
2. Once at least two different creators share their envisioning of the segment,
a countdown will start for other creators to hurry up and get any more in.
3. When that ends, voting begins. Anybody (creators and people who just read it) can vote on the entries till the new countdown ends.
4. The winner's entry is published and who made it is revealed.

Each creator can optionally include 1 plot idea and title for an upcoming episode at the bottom of their segment submission.
- If your segment wins the vote, your plot will be added to a list of upcoming episodes.
- If it loses, you can include it with the next segment you write to try again.
- Make sure it's marked clearly as not being a part of your segment and keep it short/simple.
(Try this template: [Plot Suggestion: "Title" - Idea])

## Episode
Each Episode is a set of 7 scenes that share a common plot point. This can deal with the story arc of each season or be a fun adventure episode.
(Think of it similarily to TV shows like Steven Universe.
SU had episodes about Birthday Parties alongside a giant arc about a thing that could distroy the planet
plus fun episodes could still include some bits of info that add to the arc without being entirely about it.) 

If it's a big episode, two separate entries can be combined to make a 2-parter.

## Seasons
25 episodes that cover a big story. Season 1 is about Cosmcala finding the secret that her parent's hid while learning her new AU powers.

When each season is completed, I'll turn the full season into an free eBook separated by episode chapters, cleaned up a bit
(things like Grammar, Spelling, connecting words, etc.)
, given a special Season Name to describe the Story Arc, a Credits page featuring anybody who helped, and a Recap of what happened.

# Release Schedule
1. Episode is made.
2. Creators will have 12 hours after the first two scene drafts are submitted to make sure they get theirs in.
3. Everybody will have 12 hours to vote after that.
4. Repeat 2 and 3 until the episode is filled and Story Wars marks it as Finished.
5. New episode is made and repeating the whole process.

As the first countdown only begins once a minimum of 2 drafts are submitted, we can't guarantee each episodes final release
but assuming it goes to plan, an episode should take approx. 1 week to complete[^2] and a full season would take about half of a year.
Each season will have a short 2-4 month hiatus in-between and no episodes will be started from Thanksgiving to the New Years for the holidays
(if one is in progress, it will be finished first).

[^1]: Technically speaking, it's actually 8 but the first scene doesn't have the countdowns assoicated (only scenes 2-8 so 7 scenes).
[^2]: 1 scene a day (12 hours for writing period and 12 for voting period) and 7 scenes total.